import argparse
import sys
from typing import Optional
from typing import Sequence


def find_executed_cell(filename):
    """Read the file and check whether there is a line with "execution_count" and a non null value."""
    stream = open(filename, encoding="utf8", errors='ignore')

    executed_cells = [line.strip("\n,\" ") for line in stream if "execution_count" in line and "null" not in line ] 

    return len(executed_cells)



def main(argv: Optional[Sequence[str]] = None) -> int:
    """First argument in command line is expected to be a list of filenames
    
    As the purpose of this file is to be used as a pre-commit git hook, the typical list are all the files modified by the commit in progress.

    If one or more file includes a cell that has been executed, the function returns 1 and print on screen the incriminated file(s).
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*', help='Filenames to check.')
    args = parser.parse_args(argv)

    filenames_with_executed_cells = []

    for filename in args.filenames:
        if find_executed_cell(filename):
            filenames_with_executed_cells.append(filename)

    if filenames_with_executed_cells:
        print("Commit rejected: the following files feature executed cells: \n\t- {}".format("\n\t- ".join(filenames_with_executed_cells)))
        return 1
    
    return 0


if __name__ == '__main__':
    sys.exit(main())
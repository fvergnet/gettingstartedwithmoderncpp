template<class IntT>
inline typename PrintIntHelper<IntT>::return_type PrintIntHelper<IntT>::Do(IntT value)
{
    return static_cast<return_type>(value);
};


template<class T>
inline auto PrintInt(T value)
{
    return PrintIntHelper<T>::Do(value);
}


//! Returns `number` * (2 ^ `exponent`) 
template<class IntT>
IntT times_power_of_2(IntT number, int exponent)
{
    constexpr IntT two = static_cast<IntT>(2);
    
    while (exponent > 0)
    { 
        IntT product;
        
        if (__builtin_mul_overflow(number, two, &product))
        {
            std::cout << "OVERFLOW for number " << number << std::endl;
            throw Error("Overflow! (in times_power_of_2())");
        }

        number = product;
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= two;
        exponent += 1 ; 
    }
    
    return number;
}
    

template<class IntT>
inline IntT round_as_int(double x)
{
    return static_cast<IntT>(std::round(x));
}


template<class IntT>
inline IntT max_int(int Nbits)
{ 
    constexpr IntT one = static_cast<IntT>(1);
    return (times_power_of_2(one, Nbits) - one);
}

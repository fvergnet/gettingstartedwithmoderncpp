Table of Contents
=================

- [About this tutorial](#about-this-tutorial)
- [Goal of this tutorial](#goal-of-this-tutorial)
- [How to run it?](#how-to-run-it-)


## About this tutorial

This tutorial is heavily inspired from a C++ tutorial created by David Chamont (CNRS) that was given as a lecture with the help of Vincent Rouvreau (Inria) in 2016; latest version of this tutorial used as the basis of current one may be found [there](https://gitlab.inria.fr/FormationCpp/DebuterEnCpp).

Current version provides two major modifications:

* The tutorial is now in english.
* Jupyter notebooks using [Xeus-cling kernel](https://github.com/QuantStack/xeus-cling) are now used, thus enabling a sort of interpreted C++ which is rather helpful for teaching it (even if it is clearly not yet mature...)

I have rewritten entirely and modified heavily several chapters, but the backbone remains heavily indebted to David and Vincent and the TP is still very similar to the original one.

As the original tutorial, the present lecture is released under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/) licence.


## Goal of this tutorial

This is an introductory lecture to the modern way of programming C++; at the end of it you should:

* Understand the syntax and basic mechanisms of the C++ language in version 14/17.
* Know the different programming styles.
* Know of and be able to use the most useful part of the standard library.
* Be aware of many good programming practices in C++.


## How to run it?

### Local installation

As this tutorial relies heavily on [Xeus-cling kernel](https://github.com/QuantStack/xeus-cling), the only requirement is to install this environment on your machine.

Unfortunately, the support of this kernel is still experimental on Windows; I advise you to use any variant of Linux or macOS (for Windows 10 users you may [install Ubuntu in your Windows session](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows); I haven't tried this myself).

Should the procedure described below not work at some point I invite you to check the link above, but at the time of this writing you need to:

* Install [miniconda3](https://conda.io/miniconda.html) environment (apparently using full-fledged anaconda may lead to conflict in dependencies).
* Create a new conda environment and activate it:

```
conda env create -f environment.yml
conda activate training_cpp_2021
```

Don't forget to activate it each time you intend to run the lecture!

**WARNING** At the moment, Conda packaging is broken for the most recent version of macOS (11.3). A [ticket](https://github.com/jupyter-xeus/xeus-cling/issues/403) has been issued and is under work; in the meanwhile use Binder or a local Docker image (see below)


* Then you can run the notebook by going **into its root directory** (or internal links won't work...) in a terminal and typing:

```
jupyter lab
```

__BEWARE__: I **strongly advise** to use a dedicated environment for the notebook: the ultimate goal is to use a real C++ environment for your project, and the anaconda environment hides the native C++ compiler on your machine.


__NOTE__: It is possible to use the notebooks directly from some IDEs like [VSCode](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/wikis/home#using-notebooks-with-vscode).

### Docker

It is possible to execute the notebooks in a Docker container by simply typing:

````
docker run -v $PWD:/home/formation/gettingstartedwithmoderncpp -p 8888:8888 --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling:latest
````

or 

````
docker run -v $PWD:/home/formation/gettingstartedwithmoderncpp -p 8888:8888 --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling-and-compilers:latest
````

Few hints for those not familiar with Docker:

* `-v` creates a mapping between local folder and the /home/formation/gettingstartedwithmoderncpp folder in the container; this enables you to edit the file from your comfy local environment and see the file edited this way in the Docker container.
* `--cap-drop=all` is a safety when you're running a Docker image not built by yourself: you're essentially blocking the few remaining operations that might impact your own environment that Docker lets by default open with the run command.
* `-p` gives the port mapping between the Docker image and your local environment.

The lengthy `registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling-and-compilers` is the name of the Docker **image**, if this image is not present locally in your environment Docker will try to fetch it from a *registry* on the Inria Gitlab. 

Then just type [http://localhost:8888/](http://localhost:8888/) in your browser to run the notebooks.


### BinderHub

A link to a BinderHub instance is given at the top of the project page; foresee few minutes to set up properly the notebooks.


## For maintainers

Informations related to CI are [here](CI.md).
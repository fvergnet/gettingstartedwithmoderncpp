To build docker images, the command shall be launched in the project root folder. They may also be created through CI/CD using Gitlab Web interface (see [here](../CI.md)).

```
docker build -f docker/<dockerfile> .
```

The .dockerignore file is used to reduce docker image size by keeping the docker context to minimum.

There are several Dockerfiles here:

- [Dockerfile], which creates an image with a Conda environment to run properly Xeus-cling.
- [Dockerfile-full], which creates an image with a Conda environment to run properly Xeus-cling and installs as well local compilers so that they may be invoked from the notebooks (for the 6-InRealEnvironment part).
- [Dockerfile.fedora] which provides an environment with both clang and gcc installed. This notebook is not intended to run the notebooks but may be used to run the TPs.
- [Dockerfile.boost] is used along with [the notebook about third party warnings](../6-InRealEnvironment/4-ThirdParty.ipynb).

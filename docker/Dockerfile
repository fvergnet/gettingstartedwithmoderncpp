# Inspired from https://pythonspeed.com/articles/activate-conda-dockerfile
FROM continuumio/miniconda3

LABEL maintainer Vicente Mataix Ferrandiz "vicente.mataix-ferrandiz@inria.fr" and Sébastien Gilles "sebastien.gilles@inria.fr"

# Create non root user and give him the ownership of /opt/conda.
ENV USER "formation"
RUN useradd --create-home ${USER}
RUN chown ${USER}:${USER} /opt/conda
USER ${USER}

# Create the environment:
WORKDIR /home/${USER}/gettingstartedwithmoderncpp
COPY environment.yml .
RUN conda env create -f environment.yml 

# Make RUN commands use the new environment:
SHELL ["/bin/bash", "-c"]

# The code to run when container is started:
# Using informations from https://hub.docker.com/r/continuumio/miniconda3 and options used by Vicente in former Docker file based on Ubuntu.
ENTRYPOINT [ "conda", "run", "--no-capture-output", "-n", "training_cpp_2021", "jupyter", "lab", "--port=8888", "--ip=0.0.0.0", "--no-browser","--allow-root","--NotebookApp.token=''"]

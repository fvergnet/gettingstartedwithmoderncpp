All contributions are welcome.

# Git hook

If you intend to contribute, please install first the pre-commit hook:

````
cp Scripts/pre-commit .git/hooks/pre-commit
````

This hook ensure no notebook with executed cells is committed.

# Workflow

The project is led through an integration manager workflow, so when you have a contribution to share, just open a merge request.

The MR will be reviewed and integrated directly by one of the project managers.